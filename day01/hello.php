<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php

    function displayForm() {
        // heredoc
        $form = <<< ENDMARKER
        <form>
        Enter your name: <input type="text" name="name">
        <input type="submit" value="Say hello">
        </form>
ENDMARKER;
        echo $form;
    }

        if (isset($_GET['name'])) { // STATE 2&3: submission received
            $name = $_GET['name'];
            if (strlen($name) < 2 || strlen($name) > 20) { // STATE 3: submission failed
                echo "<p>Error: name must be 2-20 characters long.</p>";
                displayForm();
            } else { // STATE 2: submission succeeded
                echo "<p>Hello <b>" . $name . "</b>, nice to meet you!</p>";
            }
        } else { // STATE 1: first show
            displayForm();
        }
    ?>
</body>
</html>