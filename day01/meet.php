<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>
    <div id="centeredContent">
<?php

function displayForm($name = "", $age = "") {
    // heredoc
    $form = <<< ENDMARKER
    <form method="post">
        Name: <input type="text" name="name" value="$name"><br>
        Age: <input type="text" name="age" value="$age"><br>
        <input type="submit" value="Append to file">
    </form>
ENDMARKER;
    echo $form;
}

    if (isset($_POST['age'])) { // STATE 2&3: submission received
        $name = $_POST['name'];
        $age = $_POST['age'];
        $errorList = array();
        if (strlen($name) < 2 || strlen($name) > 20) { // STATE 3: submission failed
            array_push($errorList, "Name must be 2-20 characters long.");
            $name = "";
        }
        if (empty($age) || !is_numeric($age) || $age < 1 || $age > 150) {
            array_push($errorList, "Age must be 1-150.");
            $age = "";
        }
        if ($errorList) { // true if array is not empty
            echo "<p class=errorMessage>There were error in your submission</p>\n<ul>\n";
            foreach ($errorList as $error) {
                echo "<li class=errorMessage>$error</li>\n";
            }
            echo "</ul>\n";
            displayForm($name, $age);
        } else { // STATE 2: submission succeeded
            file_put_contents("people.txt", "$name;$age\n", FILE_APPEND);
            echo "<p>Submission successful. Line appended to 'people.txt' file.</p>";
        }
    } else { // STATE 1: first show
        displayForm();
    }
?>
    </div>
</body>
</html>